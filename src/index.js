import express from 'express';
import http from 'http';
import cors from 'cors';
import config from './config'
import Mongoose from 'mongoose';
import './models/Number';
import apiNumbers from './controllers/api/numbers'
import sendRandomNumber from  './sockets/sendRandomNumber';
Mongoose.connect(`${config.databaseUrl}/${config.database}`);
const app = express();

const server = http.createServer(app);

app.use(cors({origin: config.corsUrl}));
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/api/numbers', apiNumbers);

const timeoutId = sendRandomNumber(server);
server.listen(config.port);

export default {app, server, db: Mongoose.connection, timeoutId};
//YEET YEET YEET YEET YEET
const getSum  = jest.fn().mockImplementation(async () => 5);

const addNumber  = jest.fn();

const topNumber  = jest.fn().mockImplementation(async () => 3);

export default {getSum, addNumber, topNumber};

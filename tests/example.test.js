import request from 'supertest';
import index from '../src/index'
jest.mock('../src/services/numberService');
import services from '../src/services/numberService';
describe('Post Endpoints', () => {
  it('should create a new post', async () => {
    const res = await request(index.app)
      .post('/api/numbers')
      .send({
        number: 1,
      })
      expect(res.body).toEqual({success: true});
      expect(services.addNumber.mock.calls[0]).toEqual([1]);
  })
})

afterAll(async () => {
  // console.log(index);
  await index.server.close();
  await index.db.close();
  clearInterval(index.timeoutId);
})
import express from 'express';
import numberService from '../../services/numberService';

const router = express.Router();

router.get('/', (req,res) => {
    numberService.getSum()
        .then(rs => {
            res.json({total: rs});
            res.close()
        });
});

router.post('/', (req,res) => {
    numberService.addNumber(req.body.number);
    res.json({success: true});
    res.close();
});

router.get('/top' , (req, res) => {
    numberService.topNumber().then((rs) => {
        res.json({number: rs});
        res.close();
    });
})

export default router;
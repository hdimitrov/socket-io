import  mongoose from 'mongoose';

const Number = mongoose.model('Number');
const getSum = () => {
    return Number.find({}).then( (result) => {
        return result.reduce((accumulator, entry) => accumulator + entry.number, 0);
    });
};

const addNumber = (number) => {
    const convertedNumber = parseInt(number, 10);
    new Number({number: convertedNumber}).save();
};

const topNumber = () => {
    return Number.find({}).then(result => {
        return result.map(result => result.number).sort((a,b) => a - b).reverse().find(() => true);
    })
}


export default { getSum, addNumber, topNumber };
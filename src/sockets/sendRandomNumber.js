import socket from 'socket.io';

const sendRandomNumber = (server) => {
    const io = socket(server);
    return setInterval(() => {
        io.emit('addNumber', Math.floor(Math.random() * 101));
    }, 3000);
};

export default sendRandomNumber;
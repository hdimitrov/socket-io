export default {
    databaseUrl: 'mongodb://localhost:27017',
    database: 'numbers',
    port: 3001,
    corsUrl: 'http://localhost:3000',
};